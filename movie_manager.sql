CREATE SCHEMA movie_manager;

CREATE TABLE movies ( 
					movie_id INT(10) PRIMARY KEY AUTO_INCREMENT, 
                    movie_name VARCHAR(100), 
                    dimension VARCHAR(5), 
                    description VARCHAR(500), 
                    cast VARCHAR(500), 
                    language VARCHAR(100), 
                    release_date DATE, 
					genre VARCHAR(100)
                    );
                    
CREATE TABLE theatres (
					theatre_id INT(10) PRIMARY KEY AUTO_INCREMENT,
					theatre_name VARCHAR(70),
                    location VARCHAR(50),
                    type VARCHAR(30),
                    facilities VARCHAR(100)
                    );
                    
CREATE TABLE shows (
					show_id INT(10) PRIMARY KEY AUTO_INCREMENT,
					screen_name VARCHAR (70),
                    price INT(10),
                    timings VARCHAR(20),
                    dimension VARCHAR(10)
                    );
                    
INSERT INTO movies VALUES(1,'Bahubali', '3D','Rajamouli movie', 'Prabhas, Anushka, Rana','Telugu , Hindi, Tamil', '2017-04-28', 'Action, Fantasy');

INSERT INTO movies VALUES(2,'Guardians of the galaxy', '3D','English movie', 'Groot','English', '2017-05-16', 'Action');

INSERT INTO movies VALUES(3,'MayaBazaar', '2D','Old movie', 'old actors','Telugu', '1995-04-16', 'love, family');

INSERT INTO movies VALUES(4,'Fan','3D','Sharukh movie', 'Sharukh Khan','Hindi', '2016-04-15 ', 'Action, Thriller');

INSERT INTO movies VALUES(5,'Fate of the Furious', '3D','English movie', 'Groot','English', '2017-05-16', 'Action');

INSERT INTO movies VALUES(6,'Charile and the Chocolate Facrtory', '2D','Old movie', 'old actors','Telugu', '1995-04-16', 'love, family');
INSERT INTO movies VALUES(7,'Chicken Little', '3D','Rajamouli movie', 'Prabhas, Anushka, Rana','Telugu , Hindi, Tamil', '2017-04-28', 'Action, Fantasy');

INSERT INTO movies VALUES(8,'Stuart Little', '3D','English movie', 'Groot','English', '2017-05-16', 'Action');

INSERT INTO movies VALUES(9,'Die Hard 2.0', '2D','Old movie', 'old actors','Telugu', '1995-04-16', 'love, family');

INSERT INTO theatres VALUES (1,'Prasads','Neckalace Road', 'Multiplex', 'food court, gaming zone');                    
INSERT INTO theatres VALUES (2,'Mantra Mall','Atta put', 'Multiplex', 'food court, gaming zone');

ALTER TABLE shows ADD movie_fkid INT(10) REFERENCES movies(movie_id);                   
ALTER TABLE shows ADD theatre_fkid INT(10) REFERENCES theatres(theatre_id);                   
			
INSERT INTO shows VALUES(1,'screen 1',200,'01:30PM','3D',4,1);
INSERT INTO shows VALUES(2,'screen 1',200,'11:30AM','2D',3,1);          

ALTER TABLE movies ADD status TINYINT DEFAULT 1;
ALTER TABLE movies ADD reviews VARCHAR(200);
ALTER TABLE movies ADD ratings INT(2);
ALTER TABLE movies DROP reviews;
ALTER TABLE movies MODIFY ratings FLOAT(2,1);
ALTER TABLE movies CHANGE ratings movie_ratings FLOAT(2,1);
SELECT * FROM movies;

ALTER TABLE theatres ADD screen_nos INT(5);
ALTER TABLE theatres MODIFY screen_nos FLOAT(2,1);
ALTER TABLE theatres CHANGE screen_nos number_screens INT(5);
ALTER TABLE theatres DROP number_screens;
SELECT * FROM theatres;

ALTER TABLE shows ADD seat_lt INT(10);
ALTER TABLE shows MODIFY seat_lt FLOAT(2,1);
ALTER TABLE shows CHANGE seat_lt seat_layout INT(10);
ALTER TABLE shows DROP seat_layout;
SELECT * FROM shows;

SELECT * FROM movies where status=1;
UPDATE movies SET status=0 where movie_id in(7,3,6);
SELECT * FROM movies where status=0;
